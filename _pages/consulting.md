---
layout: page
title: Consulting
permalink: /consulting/
---

If you're running an organisation that uses F-Droid or would like to use F-Droid, you will sometimes want to have an expert. This page is a list of companies and developers who contributed to F-Droid in one way or another and are available for consulting. By contracting one of these companies or developers, your organisation can also (indirectly) support the development of F-Droid.

Keep in mind that the F-Droid ecosystem consists of several components. Thus, consulting services may offer a lot of different services, such as
  * Deployment of F-Droid within an organisation
  * Setup and maintenance of F-Droid repositories
  * Workshops for users and admins
  * Help in getting an app compliant for inclusion in the main f-droid.org repository
  * Bug fixes or feature additions to the F-Droid client and server components
  * Whitelabel solutions

Check out the descriptions to find an appropriate consultant for your needs.

### COTECH - Confidential Technologies GmbH

* __Website:__ [https://www.cotech.de/f-droid](https://www.cotech.de/f-droid)
* __Email:__ [contact@cotech.de](mailto:contact@cotech.de)
* __Phone:__ +49 531 22435119
* __Address:__ Confidential Technologies GmbH, Wilhelmsgarten 3, 38100 Braunschweig
* __Languages:__ English, German
* __Specializations:__ Custom F-Droid deployments, feature extensions, whitelabel solutions

### FINDEISEN.SYSTEMS - Hardcore IT Consulting

* __Website:__ [https://findeisen.systems](https://findeisen.systems)
* __Email:__ [mail@findeisen.systems](mailto:mail@findeisen.systems)
* __Phone:__ +49 176 41865073
* __Address:__ FINDEISEN.SYSTEMS, Siidik 6, 25980 Sylt
* __Languages:__ English, German
* __Specializations:__ Android customization, secure communication, mobile security consulting

---

If you're missing, you can
[add yourself to the list](https://gitlab.com/fdroid/fdroid-website/blob/master/_pages/consulting.md)!
Keep it sorted alphabetically, please.
